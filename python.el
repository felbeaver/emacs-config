;;; python.el --- Settings for Python language
;;; Commentary:
;;; My Emacs settings for Python language
;;;
;;; Code:
;; Setup jedi while enter python-mode
(add-hook 'python-mode-hook 'jedi:setup)
;; Enable auto-complete after typing dot
(setq jedi:complete-on-dot t)
;;; python.el ends here
