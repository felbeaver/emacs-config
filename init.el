;;; init.el --- My Emacs configuration
;;; Commentary:
;;; This is my init.el file.
;;; It doesn't contains any configuration code except Custom
;;; automatically generated settings.
;;; Other configuration files loaded using load-user-files
;;; function.
;;;
;;; Code:
;; The variable that stores current user's configuration directory
(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
	 user-emacs-directory)
	((boundp 'user-init-directory)
	 user-init-directory)
	(t "~/.emacs.d/")))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(defun load-user-files (file &rest others)
  "Load one or more files in current user's configuration directory"
  (progn
    (load-user-file file)
    (mapc 'load-user-file others)))

(load-user-files "packages.el"
		 "global.el"
		 "keys.el"
		 "theme.el"
		 "python.el")

;; Can't place this code in other config files. This code was added by Custom.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
	(jedi auto-complete flycheck free-keys highlight-indent-guides solarized-theme color-theme-sanityinc-tomorrow atom-one-dark-theme)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(window-divider-default-right-width 3))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "outline" :family "Fira Code"))))
 '(header-line ((t (:background nil :height 75)))))
;;; init.el ends here
