;;; theme.el --- Theme settings
;;; Commentary:
;;; It's my Emacs theme settings.
;;;
;;; Code:
;; global theme settings
(load-theme 'atom-one-dark t)
(fringe-mode '(15 . 15))
(global-display-line-numbers-mode)
(window-divider-mode)
(add-hook 'buffer-list-update-hook (lambda () (setq header-line-format " ")))

;; highlight-indent-guides settings
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq highlight-indent-guides-method 'character)
(setq highlight-indent-guides-character #x250a)
(setq highlight-indent-guides-responsive 'top)
;;; theme.el ends here
