;;; global.el --- Global settings
;;; Commentary:
;;; It's my general Emacs settings.
;;;
;;; Code:
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

(global-flycheck-mode)
(ac-config-default)

(setq visible-bell nil)
(setq ring-bell-function 'ignore)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;;; global.el ends here
