;;; packages.el --- Package settings
;;; Commentary:
;;; This config adds some package archives for package
;;; manager.
;;;
;;; Code:
(require 'package)

(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)
;;; packages.el ends here
