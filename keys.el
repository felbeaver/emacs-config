;;; keys.el --- Key settings
;;; Commentary:
;;; There are my key settings and key bindings for Emacs
;;;
;;; Code:
;; tabs settings
(setq custom-tab-width 4)
(defun enable-tabs  ()
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq indent-tabs-mode t)
  (setq tab-width custom-tab-width))
(add-hook 'prog-mode-hook 'enable-tabs)

;; Language-Specific Tweaks
(defun set-html-indent-offset ()
  (set (make-local-variable 'sgml-basic-offset) custom-tab-width))

(add-hook 'html-mode-hook 'set-html-indent-offset)
(add-hook 'mhtml-mode-hook 'set-html-indent-offset)

;; Make the backspace properly erase the tab instead of
;; removing 1 space at a time.
(setq backward-delete-char-untabify-method 'hungry)

(defun newline-and-indent-same-level ()
  "Insert a newline, then indent to the same column as the current line."
  (interactive)
  (let ((col (save-excursion
               (back-to-indentation)
               (current-column))))
    (newline)
    (indent-to-column col)))

(add-hook 'prog-mode-hook
		  (lambda ()
			(local-set-key (kbd "M-n") 'newline-and-indent-same-level)))
(put 'upcase-region 'disabled nil)
;;; keys.el ends here
